using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace SpecFlowProject1.StepDefinitions
{
    [Binding]
    public class Feature1StepDefinitions
    {
        [When("условие (.*)")]
        public void WhenConditionIsTrue(bool cond)
        {
            //
            Assert.IsTrue(cond);
        }

        [Then("объект работает")]
        public void ObjectIsRunning()
        {
            Assert.IsTrue(true);
        }

        [Then("объект не работает")]
        public void ObjectIsNotRunning()
        {
            Assert.IsTrue(true);
        }
    }
}
